﻿namespace AdventureCompany.Api
{
	using System.Threading.Tasks;
	using AdventureCompany.Models;
	using AdventureCompany.Models.Account;
	using AdventureCompany.Models.Data;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Identity;
	using Microsoft.AspNetCore.Mvc;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("[controller]")]
	public class AccountController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly FinancialContext _context;

		public AccountController(UserManager<ApplicationUser> userManager, FinancialContext applicationDbContext)
		{
			_userManager = userManager;
			_context = applicationDbContext;
		}

		[AllowAnonymous]
		[HttpPost("[action]")]
		public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
		{
			if (ModelState.IsValid)
			{
				var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
				var result = await _userManager.CreateAsync(user, model.Password);
				if (result.Succeeded)
				{
					return Ok();
				}
				AddErrors(result);
			}

			// If we got this far, something failed.
			return BadRequest(ModelState);
		}

		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("[action]")]
		public async Task<ApplicationUser> GetUser()
		{
			var user = await _userManager.GetUserAsync(User);

			return user;
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError(string.Empty, error.Description);
			}
		}
	}
}
