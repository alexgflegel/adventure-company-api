﻿namespace AdventureCompany.Api
{
	using AdventureCompany.Models.Data;
	using AdventureCompany.Models.Financial;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.EntityFrameworkCore;
	using System.Linq;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class CharacterController : Controller
	{
		private readonly FinancialContext _context;

		public CharacterController(FinancialContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Returns a specific character by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("{id}")]
		public Character Get(int id)
		{
			var company = _context.Characters.Where(c => c.CharacterId == id)
					.Include(ct => ct.Transactions)
					.Include(e => e.EncounterCharacters)
				.FirstOrDefault();

			return company;
		}

		/// <summary>
		/// Adds a character to the database and returns the object with its new ids
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPost]
		public Character Post([FromBody]Character value)
		{
			//have to set the id to 0 or it'll complain about inserting an ID when that feature is disabled
			value.CharacterId = 0;

			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}

		/// <summary>
		/// Updates a character in the database and returns the object with its new ids
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		[HttpPut("{id}")]
		public Character Put(int id, [FromBody]Character value)
		{
			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}
	}
}
