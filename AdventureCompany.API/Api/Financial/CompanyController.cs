﻿namespace AdventureCompany.Api
{
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading.Tasks;
	using AdventureCompany.Models;
	using AdventureCompany.Models.Data;
	using AdventureCompany.Models.Financial;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Identity;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.EntityFrameworkCore;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("[controller]")]
	public class CompanyController : Controller
	{
		private readonly FinancialContext _context;
		private readonly UserManager<ApplicationUser> _userManager;

		public CompanyController(FinancialContext context, UserManager<ApplicationUser> userManager)
		{
			_context = context;
			_userManager = userManager;
		}

		/// <summary>
		/// Returns a list of the user's companies
		/// </summary>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet]
		public async Task<CompanyIndexViewModel> GetIndex()
		{
			var companyLists = new CompanyIndexViewModel();

			var user = await _userManager.GetUserAsync(User);

			companyLists.MyCompanies = _context.CompanyPermissions.Where(w => w.UserId == user.Id && w.Permission == CompanyPermission.PermissionLevel.Owner).Select(s => s.Company).ToList();
			companyLists.SharedCompanies = new List<Company>();

			return companyLists;
		}

		/// <summary>
		/// Returns a specific company object by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("{id}")]
		public Company Get(int id)
		{
			var company = _context.Companies.Where(c => c.CompanyId == id)
				.Include(c => c.CompanyPermissions)
				.Include(c => c.Characters)
					.ThenInclude(ct => ct.Transactions)
				.Include(c => c.Encounters)
					.ThenInclude(e => e.Loots)
						.ThenInclude(ct => ct.Transactions)
				.Include(f => f.Encounters)
					.ThenInclude(e => e.EncounterCharacters)
				.Include(f => f.Encounters)
					.ThenInclude(e => e.Transactions)
				.FirstOrDefault();

			return company;
		}

		/// <summary>
		/// Adds a new company object to the database and returns the object with new ids
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPost]
		public async Task<Company> Post([FromBody]Company value)
		{
			var user = await _userManager.GetUserAsync(User);

			//have to set the id to 0 or it'll complain about inserting an ID when that feature is disabled
			value.CompanyId = 0;

			_context.Attach(value);

			value.ModifyCompany(_context);
			_context.CompanyPermissions.Add(new CompanyPermission() { Company = value, UserId = user.Id });
			_context.SaveChanges();

			return value;
		}

		/// <summary>
		/// Updates a company and returns it with updated ids
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPut("{id}")]
		public Company Put(int id, [FromBody]Company value)
		{
			value.ReconnectChildren();

			_context.Attach(value);

			value.ModifyCompany(_context);

			_context.SaveChanges();

			return value;
		}
	}
}
