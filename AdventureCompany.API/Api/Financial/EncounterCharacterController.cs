﻿namespace AdventureCompany.Api
{
	using AdventureCompany.Models.Data;
	using AdventureCompany.Models.Financial;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Mvc;
	using System.Linq;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class EncounterCharacterController : Controller
	{
		private readonly FinancialContext _context;

		public EncounterCharacterController(FinancialContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Gets a specific encounter character record by character and encounter ids
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("{id}")]
		public EncounterCharacter Get(int characterId, int encounterId)
		{
			var company = _context.EncounterCharacters.Where(c => c.CharacterId == characterId && c.EncounterId == encounterId)
				.FirstOrDefault();

			return company;
		}

		/// <summary>
		/// Ads a new encounter character to the database and returns the object with updated ids
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPost]
		public EncounterCharacter Post([FromBody]EncounterCharacter value)
		{
			//have to set the id to 0 or it'll complain about inserting an ID when that feature is disabled
			value.CharacterId = 0;

			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}

		/// <summary>
		/// Updates an encounter character and returns it with updated ids
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		[HttpPut("{id}")]
		public EncounterCharacter Put(int id, [FromBody]EncounterCharacter value)
		{
			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}
	}
}
