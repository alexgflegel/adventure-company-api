﻿namespace AdventureCompany.Api
{
	using AdventureCompany.Models.Data;
	using AdventureCompany.Models.Financial;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.EntityFrameworkCore;
	using System.Linq;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class EncounterController : Controller
	{
		private readonly FinancialContext _context;

		public EncounterController(FinancialContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Returns a specific encounter object with children attached
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("{id}")]
		public Encounter Get(int id)
		{
			var company = _context.Encounters.Where(c => c.EncounterId == id)
					.Include(ct => ct.Transactions)
					.Include(e => e.EncounterCharacters)
					.Include(l => l.Loots)
				.FirstOrDefault();

			return company;
		}

		/// <summary>
		/// Adds a new encounter and returns it with new ids
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPost]
		public Encounter Post([FromBody]Encounter value)
		{
			//have to set the id to 0 or it'll complain about inserting an ID when that feature is disabled
			value.EncounterId = 0;

			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}

		/// <summary>
		/// Updates an encounter and returns it with updated ids
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		[HttpPut("{id}")]
		public Encounter Put(int id, [FromBody]Encounter value)
		{
			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}
	}
}
