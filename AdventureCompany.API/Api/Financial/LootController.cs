﻿namespace AdventureCompany.Api
{
	using AdventureCompany.Models.Data;
	using AdventureCompany.Models.Financial;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Mvc;
	using System.Linq;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class LootController : Controller
	{
		private readonly FinancialContext _context;

		public LootController(FinancialContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Returns a loot by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("{id}")]
		public Loot Get(int id)
		{
			var company = _context.Loots.Where(c => c.LootId == id)
				.FirstOrDefault();

			return company;
		}

		/// <summary>
		/// Adds a new loot and returns it with new ids
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPost]
		public Loot Post([FromBody]Loot value)
		{
			//have to set the id to 0 or it'll complain about inserting an ID when that feature is disabled
			value.LootId = 0;

			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}

		/// <summary>
		/// Updates a loot and returns it with updates ids
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		[HttpPut("{id}")]
		public Loot Put(int id, [FromBody]Loot value)
		{
			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}
	}
}
