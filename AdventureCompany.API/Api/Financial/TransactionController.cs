﻿namespace AdventureCompany.Api
{
	using AdventureCompany.Models.Data;
	using AdventureCompany.Models.Financial;
	using AspNet.Security.OAuth.Validation;
	using Microsoft.AspNetCore.Authorization;
	using Microsoft.AspNetCore.Mvc;
	using System.Linq;

	[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
	[Route("[controller]")]
	public class TransactionController : Controller
	{
		private readonly FinancialContext _context;

		public TransactionController(FinancialContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Returns a transaction by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpGet("{id}")]
		public Transaction Get(int id)
		{
			var company = _context.Transactions.Where(c => c.TransactionId == id)
				.FirstOrDefault();

			return company;
		}

		/// <summary>
		/// Adds a new transaction and returns it with new ids
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
		[HttpPost]
		public Transaction Post([FromBody]Transaction value)
		{
			//have to set the id to 0 or it'll complain about inserting an ID when that feature is disabled
			value.TransactionId = 0;

			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}

		/// <summary>
		/// Updates a transaction and returns it with updated ids
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		[HttpPut("{id}")]
		public Transaction Put(int id, [FromBody]Transaction value)
		{
			_context.Attach(value);

			_context.SaveChanges();

			return value;
		}
	}
}
