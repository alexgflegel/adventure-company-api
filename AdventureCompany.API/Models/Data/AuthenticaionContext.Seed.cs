﻿namespace AdventureCompany.Models.Data
{
	using AdventureCompany.Models;
	using AdventureCompany.Models.Financial;
	using Microsoft.AspNetCore.Identity;
	using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
	using Microsoft.EntityFrameworkCore;
	using System;
	using System.Linq;

	public partial class AuthenticationContext
	{
		public void Initialize(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			if (Database.EnsureCreated())
			{
				SeedData(userManager, roleManager);
			}
		}

		private void SeedData(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			if (!roleManager.RoleExistsAsync("Administrator").Result)
			{
				var role = new IdentityRole
				{
					Name = "Administrator"
				};
				//role.Description = "Perform all the operations.";
				var roleResult = roleManager.CreateAsync(role).Result;
			}

			/*
			AuthenticationContext context = serviceProvider.GetService<AuthenticationContext>();
			UserManager<ApplicationUser> _userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

			string[] roles = new string[] { "Owner", "Administrator", "Manager", "Editor", "Buyer", "Business", "Seller", "Subscriber" };

			foreach (string role in roles)
			{
				var roleStore = new RoleStore<IdentityRole>(context);

				if (!context.Roles.Any(r => r.Name == role))
				{
					roleStore.CreateAsync(new IdentityRole(role));
				}
			}
			//var user = new

			var user = new ApplicationUser
			{
				Email = "xxxx@example.com",
				NormalizedEmail = "XXXX@EXAMPLE.COM",
				UserName = "Owner",
				NormalizedUserName = "OWNER",
				PhoneNumber = "+111111111111",
				EmailConfirmed = true,
				PhoneNumberConfirmed = true,
				SecurityStamp = Guid.NewGuid().ToString("D")
			};

			if (!context.Users.Any(u => u.UserName == user.UserName))
			{
				var password = new PasswordHasher<ApplicationUser>();
				var hashed = password.HashPassword(user, "secret");
				user.PasswordHash = hashed;

				var userStore = new UserStore<ApplicationUser>(context);
				var result = userStore.CreateAsync(user);

			}

			_userManager.AddToRolesAsync(user, roles);
			*/
		}
	}
}
