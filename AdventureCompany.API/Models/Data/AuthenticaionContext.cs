﻿namespace AdventureCompany.Models.Data
{
	using AdventureCompany.Models;
	using AdventureCompany.Models.Financial;
	using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
	using Microsoft.EntityFrameworkCore;

	public partial class AuthenticationContext : IdentityDbContext<ApplicationUser>
	{
		public AuthenticationContext(DbContextOptions<AuthenticationContext> options) : base(options)
		{
		}
	}
}
