﻿namespace AdventureCompany.Models.Data
{
	using AdventureCompany.Models;
	using AdventureCompany.Models.Financial;
	using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
	using Microsoft.EntityFrameworkCore;

	public partial class FinancialContext : DbContext
	{
		public FinancialContext(DbContextOptions<FinancialContext> options) : base(options)
		{
		}

		public DbSet<Character> Characters { get; set; }
		public DbSet<Company> Companies { get; set; }
		public DbSet<CompanyPermission> CompanyPermissions { get; set; }
		public DbSet<Encounter> Encounters { get; set; }
		public DbSet<EncounterCharacter> EncounterCharacters { get; set; }
		public DbSet<Loot> Loots { get; set; }
		public DbSet<Transaction> Transactions { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Character>().ToTable(nameof(Character));
			modelBuilder.Entity<Company>().ToTable(nameof(Company));
			modelBuilder.Entity<CompanyPermission>().ToTable(nameof(CompanyPermission));
			modelBuilder.Entity<Encounter>().ToTable(nameof(Encounter));
			modelBuilder.Entity<EncounterCharacter>().ToTable(nameof(EncounterCharacter));
			modelBuilder.Entity<Loot>().ToTable(nameof(Loot));
			modelBuilder.Entity<Transaction>().ToTable(nameof(Transaction));

			modelBuilder.Entity<EncounterCharacter>().HasKey(composite => new { composite.CharacterId, composite.EncounterId });

			modelBuilder.Entity<EncounterCharacter>()
				.HasOne(e => e.Character)
				.WithMany(c => c.EncounterCharacters);

			modelBuilder.Entity<EncounterCharacter>()
				.HasOne(e => e.Encounter)
				.WithMany(e => e.EncounterCharacters);

			modelBuilder.Entity<Character>()
				.HasOne(e => e.Company)
				.WithMany(e => e.Characters);

			modelBuilder.Entity<CompanyPermission>().HasKey(composite => new { composite.CompanyId, composite.UserId });

			modelBuilder.Entity<CompanyPermission>()
				.HasOne(e => e.Company)
				.WithMany(c => c.CompanyPermissions);

			modelBuilder.Entity<Transaction>()
				.HasOne(e => e.Character)
				.WithMany(c => c.Transactions);

			modelBuilder.Entity<Transaction>()
				.HasOne(e => e.Encounter)
				.WithMany(e => e.Transactions);

			modelBuilder.Entity<Transaction>()
				.HasOne(e => e.Loot)
				.WithMany(e => e.Transactions);

			modelBuilder.Entity<Loot>()
				.HasOne(e => e.Encounter)
				.WithMany(e => e.Loots);

			modelBuilder.Entity<Encounter>()
				.HasOne(e => e.Company)
				.WithMany(e => e.Encounters);

			base.OnModelCreating(modelBuilder);
		}
	}
}
