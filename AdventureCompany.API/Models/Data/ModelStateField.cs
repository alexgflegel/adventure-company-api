﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AdventureCompany.Models.Data
{
	public class ModelStateField
	{
		[NotMapped]
		public EntityState? ModelState { get; set; } = EntityState.Unchanged;
	}
}
