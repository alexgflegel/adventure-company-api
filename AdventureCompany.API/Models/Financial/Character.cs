namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Newtonsoft.Json;
	using System.Diagnostics.CodeAnalysis;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("Character")]
	public partial class Character : ModelStateField
	{
		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Character()
		{
			EncounterCharacters = new HashSet<EncounterCharacter>();
		}

		/*
		 * this is meant to be able to change who is:
		 * - listed in the management screens
		 * - Contributing to the Party Pool
		 * -
		*/
		public enum State
		{
			Active = 0,
			Inactive = 1,
			Dead = 2
		}

		//[Key]
		public int CharacterId { get; set; }

		[StringLength(50)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		[StringLength(50)]
		[Display(Name = "Description")]
		public string Description { get; set; }

		[Display(Name = "Status")]
		public State? Status { get; set; }

		public int? CompanyId { get; set; }

		[StringLength(50)]
		public string UserId { get; set; }

		[JsonIgnore]
		public virtual Company Company { get; set; }

		[JsonIgnore]
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<EncounterCharacter> EncounterCharacters { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Transaction> Transactions { get; set; }
	}
}