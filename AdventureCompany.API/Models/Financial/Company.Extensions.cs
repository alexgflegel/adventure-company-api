﻿namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Microsoft.EntityFrameworkCore;
	using System.Collections.Generic;
	using System.Linq;

	public partial class Company
	{
		/// <summary>
		/// Connects children to their parents properly and zeroes out new ids once connected
		/// </summary>
		public void ReconnectChildren()
		{
			foreach (var character in Characters)
			{
				character.Company = this;

				//characters need to reconnect to encounters and transactions
				foreach (var encounterCharacter in Encounters.SelectMany(s => s.EncounterCharacters).Where(w => w.CharacterId == character.CharacterId))
				{
					if (character.CharacterId <= 0)
						encounterCharacter.CharacterId = 0;

					character.EncounterCharacters.Add(encounterCharacter);
					encounterCharacter.Character = character;
					//ignore the encounter id
				}

				foreach (var transaction in character.Transactions)
				{
					if (character.CharacterId <= 0)
						transaction.CharacterId = 0;

					transaction.Character = character;

					if (transaction.TransactionId <= 0)
						transaction.TransactionId = 0;
				}

				foreach (var transaction in Encounters.SelectMany(s => s.Loots.SelectMany(ss => ss.Transactions)).Where(w => w.CharacterId == character.CharacterId))
				{
					if (character.CharacterId <= 0)
						transaction.CharacterId = 0;

					character.Transactions.Add(transaction);
					transaction.Character = character;
					//ignore the transaction id
				}

				if (character.CharacterId <= 0)
					character.CharacterId = 0;
			}

			foreach (var encounter in Encounters)
			{
				encounter.Company = this;

				foreach (var transaction in encounter.Transactions)
				{
					if (transaction.EncounterId <= 0)
						transaction.EncounterId = 0;

					transaction.Encounter = encounter;

					if (transaction.TransactionId <= 0)
						transaction.TransactionId = 0;
				}

				foreach (var encounterCharacter in encounter.EncounterCharacters)
				{
					if (encounterCharacter.EncounterId <= 0)
						encounterCharacter.EncounterId = 0;

					encounterCharacter.Encounter = encounter;
				}

				foreach (var loot in encounter.Loots)
				{
					if (loot.EncounterId <= 0)
						loot.EncounterId = 0;

					loot.Encounter = encounter;

					foreach (var transaction in loot.Transactions)
					{
						//todo fix duplicate entries for transactions with loot and characters
						if (transaction.LootId <= 0)
							transaction.LootId = 0;

						transaction.Loot = loot;

						if (transaction.TransactionId <= 0)
							transaction.TransactionId = 0;

						if ((transaction.CharacterId ?? 1) <= 0)
							transaction.CharacterId = 0;
					}

					if (loot.LootId <= 0)
						loot.LootId = 0;
				}

				if (encounter.EncounterId <= 0)
					encounter.EncounterId = 0;
			}
		}

		/// <summary>
		/// Iterates through each object and sets the entity state based on what was passed in from angular
		/// </summary>
		/// <param name="_context"></param>
		public void ModifyCompany(FinancialContext _context)
		{
			_context.Entry(this).State = ModelState ?? EntityState.Unchanged;

			foreach (var character in Characters)
			{
				_context.Entry(character).State = character.ModelState ?? EntityState.Unchanged;

				foreach (var transaction in character.Transactions)
				{
					_context.Entry(transaction).State = transaction.ModelState ?? EntityState.Unchanged;
				}
			}

			foreach (var encounter in Encounters)
			{
				_context.Entry(encounter).State = encounter.ModelState ?? EntityState.Unchanged;

				foreach (var transaction in encounter.Transactions)
				{
					_context.Entry(transaction).State = transaction.ModelState ?? EntityState.Unchanged;
				}

				foreach (var encounterCharacter in encounter.EncounterCharacters)
				{
					_context.Entry(encounterCharacter).State = encounterCharacter.ModelState ?? EntityState.Unchanged;
				}

				foreach (var loot in encounter.Loots)
				{
					_context.Entry(loot).State = loot.ModelState ?? EntityState.Unchanged;

					foreach (var transaction in loot.Transactions)
					{
						_context.Entry(transaction).State = transaction.ModelState ?? EntityState.Unchanged;
					}
				}
			}
		}
	}
}
