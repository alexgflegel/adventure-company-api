namespace AdventureCompany.Models.Financial
{
	using Newtonsoft.Json;
	using System.Collections.Generic;
	using System.Diagnostics.CodeAnalysis;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using AdventureCompany.Models.Data;

	[Table("Company")]
	public partial class Company : ModelStateField
	{
		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Company()
		{
			Characters = new HashSet<Character>();
			Encounters = new HashSet<Encounter>();
		}

		//[Key]
		public int CompanyId { get; set; }

		[StringLength(50)]
		[Display(Name = "Company Name")]
		public string Name { get; set; }

		[StringLength(50)]
		[Display(Name = "Description")]
		public string Description { get; set; }

		[StringLength(50)]
		[Display(Name = "Art Asset")]
		public string ArtAsset { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Character> Characters { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Encounter> Encounters { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<CompanyPermission> CompanyPermissions { get; set; }
	}
}