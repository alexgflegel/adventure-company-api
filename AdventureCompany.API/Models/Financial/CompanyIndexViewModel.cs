﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventureCompany.Models.Financial
{
	public class CompanyIndexViewModel
	{
		public List<Company> MyCompanies { get; set; }
		public List<Company> SharedCompanies { get; set; }
	}
}
