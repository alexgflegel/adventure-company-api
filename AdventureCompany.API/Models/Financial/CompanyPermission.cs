﻿namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Newtonsoft.Json;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public class CompanyPermission : ModelStateField
	{
		//[Key]
		//[Column(Order = 0)]
		[StringLength(128)]
		public string UserId { get; set; }

		//[Key]
		//[Column(Order = 1)]
		public int CompanyId { get; set; }

		[JsonIgnore]
		public virtual Company Company { get; set; }

		//[ScriptIgnore(ApplyToOverrides = true)]
		//public virtual ApplicationUser ApplicationUser { get; set; }

		public PermissionLevel Permission { get; set; }

		public enum PermissionLevel
		{
			Owner = 0,
			Read = 1,
			Write = 2
		}
	}
}