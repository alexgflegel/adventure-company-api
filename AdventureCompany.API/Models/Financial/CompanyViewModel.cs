﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventureCompany.Models.Financial
{
	public class CompanyViewModel
	{
		public Company Company { get; set; }
		public List<CompanyPermission> CompanyPermissions { get; set; }
		public List<Encounter> Encounters { get; set; }
		public List<Character> Characters { get; set; }
		public List<EncounterCharacter> EncounterCharacters { get; set; }
		public List<Loot> Loots { get; set; }
		public List<Transaction> Transactions { get; set; }
	}
}
