namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Newtonsoft.Json;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("Encounter")]
	public partial class Encounter : ModelStateField
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Encounter()
		{
			EncounterCharacters = new HashSet<EncounterCharacter>();
			Loots = new HashSet<Loot>();
		}

		//[Key]
		public int EncounterId { get; set; }

		[StringLength(50)]
		[Display(Name = "Description")]
		public string Description { get; set; }

		[StringLength(50)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		[Display(Name = "Time Stamp")]
		public DateTime? Timestamp { get; set; }

		public int? CompanyId { get; set; }

		[JsonIgnore]
		public virtual Company Company { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<EncounterCharacter> EncounterCharacters { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Loot> Loots { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Transaction> Transactions { get; set; }
	}
}