namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Newtonsoft.Json;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("EncounterCharacter")]
	public partial class EncounterCharacter : ModelStateField
	{
		//[Key]
		//[Column(Order = 0)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int EncounterId { get; set; }

		//[Key]
		//[Column(Order = 1)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int CharacterId { get; set; }

		[Display(Name = "Shares")]
		public int? Shares { get; set; }

		[JsonIgnore]
		public virtual Character Character { get; set; }

		[JsonIgnore]
		public virtual Encounter Encounter { get; set; }
	}
}