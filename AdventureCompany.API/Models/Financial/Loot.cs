namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Newtonsoft.Json;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("Loot")]
	public partial class Loot : ModelStateField
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Loot()
		{
			Transactions = new HashSet<Transaction>();
		}

		//[Key]
		public int LootId { get; set; }

		public int? EncounterId { get; set; }

		[StringLength(50)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		[StringLength(50)]
		[Display(Name = "Description")]
		public string Description { get; set; }

		[Display(Name = "Value")]
		public decimal? Value { get; set; }

		[Display(Name = "Quantity")]
		public int? Quantity { get; set; }

		[JsonIgnore]
		public virtual Encounter Encounter { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<Transaction> Transactions { get; set; }
	}
}