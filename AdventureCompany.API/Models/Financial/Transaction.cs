namespace AdventureCompany.Models.Financial
{
	using AdventureCompany.Models.Data;
	using Newtonsoft.Json;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("Transaction")]
	public partial class Transaction : ModelStateField
	{
		//[Key]
		public int TransactionId { get; set; }

		public int? CharacterId { get; set; }

		public int? EncounterId { get; set; }

		public int? LootId { get; set; }

		/// <summary>
		/// Amount is the physical currency transfer
		/// </summary>
		[Display(Name = "Amount")]
		public decimal? Amount { get; set; }

		[Display(Name = "Quantity")]
		public int? Quantity { get; set; }

		/// <summary>
		/// Value is the estimated value of the transfer
		/// </summary>
		[Display(Name = "Value")]
		public decimal? Value { get; set; }

		[Display(Name = "Time Stamp")]
		public DateTime? Timestamp { get; set; }

		/// <summary>
		/// A copy of the name of the event
		/// </summary>
		[StringLength(50)]
		[Display(Name = "Name")]
		public string Name { get; set; }

		/// <summary>
		/// A copy of the description of the event
		/// </summary>
		[StringLength(50)]
		[Display(Name = "Description")]
		public string Description { get; set; }

		[JsonIgnore]
		public virtual Character Character { get; set; }

		[JsonIgnore]
		public virtual Loot Loot { get; set; }

		[JsonIgnore]
		public virtual Encounter Encounter { get; set; }
	}
}
