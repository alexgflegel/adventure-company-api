﻿namespace AdventureCompany
{
	using AdventureCompany.Models;
	using AdventureCompany.Models.Data;
	using Microsoft.AspNetCore.Builder;
	using Microsoft.AspNetCore.Hosting;
	using Microsoft.AspNetCore.HttpOverrides;
	using Microsoft.AspNetCore.Identity;
	using Microsoft.EntityFrameworkCore;
	using Microsoft.Extensions.Configuration;
	using Microsoft.Extensions.DependencyInjection;
	using Microsoft.Extensions.Logging;
	using OpenIddict.Abstractions;

	public class Startup
	{
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		public IConfigurationRoot Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			// Add framework services.
			services.AddCors();
			services.AddMvc();/*.AddJsonOptions(options => {
				options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
			})*/

			services.AddDbContext<AuthenticationContext>(options =>
			{
				options.UseSqlServer(Configuration.GetConnectionString("AuthenticationContext"));
				//options.UseNpgsql(Configuration.GetConnectionString("AuthenticationPostgresContext"));

				// Register the entity sets needed by OpenIddict.
				// Note: use the generic overload if you need to replace the default OpenIddict entities.
				options.UseOpenIddict();
			});

			services.AddDbContext<FinancialContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("FinancialContext"))
				//options.UseNpgsql(Configuration.GetConnectionString("FinancialPostgresContext"))
			);

			// Register the Identity services.
			services.AddIdentity<ApplicationUser, IdentityRole>()
				.AddEntityFrameworkStores<AuthenticationContext>()
				.AddDefaultTokenProviders();

			services.Configure<IdentityOptions>(options =>
			{
				options.ClaimsIdentity.UserNameClaimType = OpenIddictConstants.Claims.Name;
				options.ClaimsIdentity.UserIdClaimType = OpenIddictConstants.Claims.Subject;
				options.ClaimsIdentity.RoleClaimType = OpenIddictConstants.Claims.Role;
			});

			//services.AddAuthentication();

			services.AddOpenIddict().AddCore(options =>
			{
				// Register the Entity Framework stores.
				options.UseEntityFrameworkCore().UseDbContext<AuthenticationContext>();
			})
			.AddServer(options =>
			{
				options.UseMvc();
				options.EnableTokenEndpoint("/authorization/exchange");

				options.AllowPasswordFlow()
					.AllowRefreshTokenFlow();

				options.EnableRequestCaching();

				options.RegisterScopes(
					OpenIddictConstants.Scopes.Email,
					OpenIddictConstants.Scopes.Profile,
					OpenIddictConstants.Scopes.Roles);

				options.DisableHttpsRequirement();
				options.AcceptAnonymousClients();
			})
			.AddValidation();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, AuthenticationContext authenticationContext, FinancialContext financialContext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseBrowserLink();

				authenticationContext.Initialize(userManager, roleManager);
				financialContext.Initialize();
			}
			else
			{
				//Required for Nginx hosting on a raspberry pi
				app.UseForwardedHeaders(new ForwardedHeadersOptions
				{
					ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
				});

				app.UseExceptionHandler("/Home/Error");
			}

			// Add a middleware used to validate access tokens and protect the API endpoints.
			//app.UseOAuthValidation();

			// Alternatively, you can also use the introspection middleware. Using it is recommended if your resource server is in a different application/separated from the authorization server.
			//
			// app.UseOAuthIntrospection(options =>
			// {
			//     options.AutomaticAuthenticate = true;
			//     options.AutomaticChallenge = true;
			//     options.Authority = "http://localhost:57739/";
			//     options.Audiences.Add("resource_server");
			//     options.ClientId = "resource_server";
			//     options.ClientSecret = "875sqd4s5d748z78z7ds1ff8zz8814ff88ed8ea4z4zzd";
			// });
			app.UseCors(builder =>
			{
				builder.WithOrigins("http://localhost:5055");
				builder.AllowAnyHeader();
				builder.AllowAnyMethod();
			});

			app.UseAuthentication();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});

		}
	}
}
