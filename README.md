# Adventure Company API

## What is this repository for?

* An app to track and distribute table-top rpg loot between player characters
* 3.0.0

## How do I get set up?

* Configure database connection strings to your server
  * MS Sql and PostgreSql are supported
* Ensure the MyGet source is configured
* Run `nuget restore`
* Once the package restore is complete the databases should create themselves with the proper model

## Contribution guidelines

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### C# Standards

* Tabs over spaces